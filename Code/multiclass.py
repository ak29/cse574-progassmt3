#Multiclass logistic regreession
from __future__ import division
import numpy as np
from scipy.io import loadmat
from scipy.optimize import minimize


def preprocess():
    
    # loads the MAT object as a Dictionary
    mat = loadmat('../Data/mnist_all.mat')

    n_feature = mat.get("train1").shape[1]
    n_sample = 0
    for i in range(10):
        n_sample = n_sample + mat.get("train" + str(i)).shape[0]
    n_validation = 1000
    n_train = n_sample - 10 * n_validation

    # Construct validation data
    validation_data = np.zeros((10 * n_validation, n_feature))
    for i in range(10):
        validation_data[i * n_validation:(i + 1) * n_validation, :] = mat.get("train" + str(i))[0:n_validation, :]

    # Construct validation label
    validation_label = np.ones((10 * n_validation, 1))
    for i in range(10):
        validation_label[i * n_validation:(i + 1) * n_validation, :] = i * np.ones((n_validation, 1))

    # Construct training data and label
    train_data = np.zeros((n_train, n_feature))
    train_label = np.zeros((n_train, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("train" + str(i)).shape[0]
        train_data[temp:temp + size_i - n_validation, :] = mat.get("train" + str(i))[n_validation:size_i, :]
        train_label[temp:temp + size_i - n_validation, :] = i * np.ones((size_i - n_validation, 1))
        temp = temp + size_i - n_validation

    # Construct test data and label
    n_test = 0
    for i in range(10):
        n_test = n_test + mat.get("test" + str(i)).shape[0]
    test_data = np.zeros((n_test, n_feature))
    test_label = np.zeros((n_test, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("test" + str(i)).shape[0]
        test_data[temp:temp + size_i, :] = mat.get("test" + str(i))
        test_label[temp:temp + size_i, :] = i * np.ones((size_i, 1))
        temp = temp + size_i

    # Delete features which don't provide any useful information for classifiers
    sigma = np.std(train_data, axis=0)
    index = np.array([])
    for i in range(n_feature):
        if (sigma[i] > 0.001):
            index = np.append(index, [i])
    train_data = train_data[:, index.astype(int)]
    validation_data = validation_data[:, index.astype(int)]
    test_data = test_data[:, index.astype(int)]

    # Scale data to 0 and 1
    train_data /= 255.0
    validation_data /= 255.0
    test_data /= 255.0

    return train_data, train_label, validation_data, validation_label, test_data, test_label

def sigmoid(z):
    # please refer to sigma value in eqn 1
    return 1.0 / (1.0 + np.exp(-z))

def mlrObjFunction(params, *args):
    """
    mlrObjFunction computes multi-class Logistic Regression error function and
    its gradient.

    Input:
        initialWeights_b: the weight vector of size (D + 1) x 10
        train_data: the data matrix of size N x D
        Y is an N * K matrix (obtained using 1-of-K encoding) of target variables with elements Ynk

    Output:
        error: the scalar value of error function of multi-class logistic regression
        error_grad: the vector of size (D+1) x 10 representing the gradient of
                    error function
    """
    n_data = train_data.shape[0]
    
    n_feature = train_data.shape[1]
    error = 0.00
    error_grad = np.zeros((n_feature + 1, 10))

    # number of classes = number of digits to be classified
    n_class = 10
    # number of training samples N
    n_train = train_data.shape[0]

    #number of testing samples
    n_test = test_data.shape[0]

    # number of validation samples
    n_validation = validation_data.shape[0]

    # number of features D+1
    n_features = train_data.shape[1]
    
    # output label matrix

    ##################
    # YOUR CODE HERE #
    ##################
    # Add bias element to input data. A coloumn with with all values = 1 is added at first position.
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    train_data_bias = np.hstack((one_array, train_data))

    # 1-of-k matrix of labels
    Y = np.zeros((n_train, 10))
    for i in range(10):
        Y[:, i] = (train_label == i).astype(int).ravel()

    #imple eqn 5
    theta_mat =  np.zeros(shape=(n_data,1));    
    theta_norm = np.zeros(shape=(n_data,1));
    theta_fin =  np.zeros(shape=(n_data,10));    
    mat0 = np.zeros((n_features + 1, 1))
    temp3 = np.zeros((1, n_features + 1))

    class_mat0 = np.zeros((1, n_features + 1))
    var0 = np.zeros((n_features + 1, 1))
    class_mat1 = np.zeros((1, n_features + 1))
    var1 = np.zeros((n_features + 1, 1))
    class_mat2 = np.zeros((1, n_features + 1))
    var2 = np.zeros((n_features + 1, 1))
    class_mat3 = np.zeros((1, n_features + 1))
    var3 = np.zeros((n_features + 1, 1))
    class_mat4 = np.zeros((1, n_features + 1))
    var4 = np.zeros((n_features + 1, 1))
    class_mat5 = np.zeros((1, n_features + 1))
    var5 = np.zeros((n_features + 1, 1))
    class_mat6 = np.zeros((1, n_features + 1))
    var6 = np.zeros((n_features + 1, 1))
    class_mat7 = np.zeros((1, n_features + 1))
    var7 = np.zeros((n_features + 1, 1))
    class_mat8 = np.zeros((1, n_features + 1))
    var8 = np.zeros((n_features + 1, 1))
    class_mat9 = np.zeros((1, n_features + 1))
    var9 = np.zeros((n_features + 1, 1))

    params = params.reshape((n_features + 1, 10))
    print "params shape"
    print params.shape
    """
    for k in range(n_data):
        temp3 = train_data_bias[k].reshape((1, 716))
        for i in range(716):
            mat0[0] = initialWeights_b[i][0]
            theta_mat[k][0] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[1] = initialWeights_b[i][1]
            theta_mat[k][1] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[2] = initialWeights_b[i][2]
            theta_mat[k][2] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[3] = initialWeights_b[i][3]
            theta_mat[k][3] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[4] = initialWeights_b[i][4]
            theta_mat[k][4] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[5] = initialWeights_b[i][5]
            theta_mat[k][5] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[6] = initialWeights_b[i][6]
            theta_mat[k][7] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[8] = initialWeights_b[i][8]
            theta_mat[k][8] = np.exp(np.dot(temp3, temp4))
        for i in range(716):
            temp4[9] = initialWeights_b[i][9]
            theta_mat[k][9] = np.exp(np.dot(temp3, temp4))
    print "some elements of bfr theta_mat "
    for j in range(20,21):
        for k in range(10):
            print(theta_mat[j][k])
    
    for j in range(n_data):
        for k in range(0,10):
            theta_norm[j] = theta_norm[j] + theta_mat[j][k]
    print "some elements of theta_norm "
    for j in range(20,21):
        print(theta_norm[j])

    for j in range(n_data):
        for k in range(0,10):
            theta_fin[j][k] = theta_mat[j][k]/theta_norm[j]
    print "some elements of theta_fin "
    for j in range(20,21):
        for k in range(10):
            print(theta_fin[j][k])
    #Impl eqn 7
    for j in range(n_data):
        for k in range(0,10):
            error = error + (Y[j][k] * (np.log((theta_fin[j][k]))))
        print "\nsum is"
        print error
    error = (-1 * error) / n_data
    print "error here"
    print error"""
"""
Script for Extra Credit Part
"""
# FOR EXTRA CREDIT ONLY
#get input data from preprocess function
train_data, train_label, validation_data, validation_label, test_data, test_label = preprocess()

# number of classes = number of digits to be classified
n_class = 10
print str(n_class)
# number of training samples N
n_train = train_data.shape[0]

# number of testing samples 
n_test = test_data.shape[0]

# number of validation samples 
n_validation = validation_data.shape[0]

# number of features D+1
n_feature = train_data.shape[1]
print "  n_feature =  " + str(n_feature)

Y = np.zeros((n_train, 10))
for i in range(10):
    Y[:, i] = (train_label == i).astype(int).ravel()

W_b = np.zeros((n_feature + 1, 10))
initialWeights_b = np.zeros((n_feature + 1, 10))
opts_b = {'maxiter': 100}

args_b = (train_data, Y)
nn_params = minimize(mlrObjFunction, initialWeights_b, jac=True, args=args_b, method='CG', options=opts_b)
W_b = nn_params.x.reshape((n_feature + 1, 10))