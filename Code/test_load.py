import numpy as np
from scipy.io import loadmat
from scipy.optimize import minimize

def sigmoid(z):
    # please refer to sigma value in eqn 1
    return 1.0 / (1.0 + np.exp(-z))

mat = loadmat('../Data/mnist_all.mat')  

n_feature = mat.get("train1").shape[1]
n_sample = 0
for i in range(10):
    n_sample = n_sample + mat.get("train" + str(i)).shape[0]
n_validation = 1000
n_train = n_sample - 10 * n_validation

# Construct validation data
validation_data = np.zeros((10 * n_validation, n_feature))
for i in range(10):
    validation_data[i * n_validation:(i + 1) * n_validation, :] = mat.get("train" + str(i))[0:n_validation, :]

# Construct validation label
validation_label = np.ones((10 * n_validation, 1))
for i in range(10):
    validation_label[i * n_validation:(i + 1) * n_validation, :] = i * np.ones((n_validation, 1))

# Construct training data and label
train_data = np.zeros((n_train, n_feature))
train_label = np.zeros((n_train, 1))
temp = 0
for i in range(10):
    size_i = mat.get("train" + str(i)).shape[0]
    train_data[temp:temp + size_i - n_validation, :] = mat.get("train" + str(i))[n_validation:size_i, :]
    train_label[temp:temp + size_i - n_validation, :] = i * np.ones((size_i - n_validation, 1))
    temp = temp + size_i - n_validation

# Construct test data and label
n_test = 0
for i in range(10):
    n_test = n_test + mat.get("test" + str(i)).shape[0]
test_data = np.zeros((n_test, n_feature))
test_label = np.zeros((n_test, 1))
temp = 0
for i in range(10):
    size_i = mat.get("test" + str(i)).shape[0]
    test_data[temp:temp + size_i, :] = mat.get("test" + str(i))
    test_label[temp:temp + size_i, :] = i * np.ones((size_i, 1))
    temp = temp + size_i

# Delete features which don't provide any useful information for classifiers
sigma = np.std(train_data, axis=0)
index = np.array([])
for i in range(n_feature):
    if (sigma[i] > 0.001):
        index = np.append(index, [i])
train_data = train_data[:, index.astype(int)]
validation_data = validation_data[:, index.astype(int)]
test_data = test_data[:, index.astype(int)]

# Scale data to 0 and 1
train_data /= 255.0
validation_data /= 255.0
test_data /= 255.0


#BLRObjective

n_class = 10

# number of training samples N
n_train = train_data.shape[0]

# number of features D+1
n_feature = train_data.shape[1]
print "elements of label " 
for i in range(20, 25):
    print train_label

"""# create label matrix.it contains labels in 1-of-k format
Y = np.zeros((n_train, n_class))
for i in range(n_class):
    Y[:, i] = (train_label == i).astype(int).ravel()

# Logistic Regression with Gradient Descent
W = np.zeros((n_feature + 1, n_class))
initialWeights = np.zeros((n_feature + 1, 1))
opts = {'maxiter': 100}
for i in range(n_class):
    labeli = Y[:, i].reshape(n_train, 1)
    args = (train_data, labeli)


train_data, labeli = args

n_data = train_data.shape[0]
n_features = train_data.shape[1]
error = 0
error_grad = np.zeros((n_features + 1, 1))

##################
# YOUR CODE HERE #
##################
# Add bias element to input data. A coloumn with with all values = 1 is added at first position.
one_array = np.ndarray([n_data,1])
one_array.fill('1')
train_data_bias = np.hstack((one_array, train_data))
# Implement equation 2. this gives scalar value error.
theta = np.zeros(shape=(n_data,1));
sum1 = 0
count = 0;
theta = sigmoid(np.dot(train_data_bias,initialWeights));


for i in range(n_data):
    print labeli[i][0];

for i in range(n_data):
    temp =  labeli[i][0]*np.log(theta[i][0]) + (1-labeli[i][0])*np.log(1-theta[i][0]);
    sum1 = sum1 + temp

error = (1/train_data_bias.shape[0]) * sum1
error_grad = (theta[0][0] - labeli[0][0])* train_data_bias[0];

for i in range(1,n_data):
    error_grad = error_grad + (theta[i][0]-labeli[i][0]) * train_data_bias[i];

print error_grad.shape;"""
