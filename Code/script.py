import numpy as np
from scipy.io import loadmat
from scipy.optimize import minimize
import pickle
from sklearn.svm import SVC

def preprocess():
    """ 
     Input:
     Although this function doesn't have any input, you are required to load
     the MNIST data set from file 'mnist_all.mat'.

     Output:
     train_data: matrix of training set. Each row of train_data contains 
       feature vector of a image
     train_label: vector of label corresponding to each image in the training
       set
     validation_data: matrix of training set. Each row of validation_data 
       contains feature vector of a image
     validation_label: vector of label corresponding to each image in the 
       training set
     test_data: matrix of training set. Each row of test_data contains 
       feature vector of a image
     test_label: vector of label corresponding to each image in the testing
       set
    """

    mat = loadmat('../Data/mnist_all.mat')  # loads the MAT object as a Dictionary

    n_feature = mat.get("train1").shape[1]
    n_sample = 0
    for i in range(10):
        n_sample = n_sample + mat.get("train" + str(i)).shape[0]
    n_validation = 1000
    n_train = n_sample - 10 * n_validation

    # Construct validation data
    validation_data = np.zeros((10 * n_validation, n_feature))
    for i in range(10):
        validation_data[i * n_validation:(i + 1) * n_validation, :] = mat.get("train" + str(i))[0:n_validation, :]

    # Construct validation label
    validation_label = np.ones((10 * n_validation, 1))
    for i in range(10):
        validation_label[i * n_validation:(i + 1) * n_validation, :] = i * np.ones((n_validation, 1))

    # Construct training data and label
    train_data = np.zeros((n_train, n_feature))
    train_label = np.zeros((n_train, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("train" + str(i)).shape[0]
        train_data[temp:temp + size_i - n_validation, :] = mat.get("train" + str(i))[n_validation:size_i, :]
        train_label[temp:temp + size_i - n_validation, :] = i * np.ones((size_i - n_validation, 1))
        temp = temp + size_i - n_validation

    # Construct test data and label
    n_test = 0
    for i in range(10):
        n_test = n_test + mat.get("test" + str(i)).shape[0]
    test_data = np.zeros((n_test, n_feature))
    test_label = np.zeros((n_test, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("test" + str(i)).shape[0]
        test_data[temp:temp + size_i, :] = mat.get("test" + str(i))
        test_label[temp:temp + size_i, :] = i * np.ones((size_i, 1))
        temp = temp + size_i

    # Delete features which don't provide any useful information for classifiers
    sigma = np.std(train_data, axis=0)
    index = np.array([])
    for i in range(n_feature):
        if (sigma[i] > 0.001):
            index = np.append(index, [i])
    train_data = train_data[:, index.astype(int)]
    validation_data = validation_data[:, index.astype(int)]
    test_data = test_data[:, index.astype(int)]

    # Scale data to 0 and 1
    train_data /= 255.0
    validation_data /= 255.0
    test_data /= 255.0

    return train_data, train_label, validation_data, validation_label, test_data, test_label


def sigmoid(z):
    return 1.0 / (1.0 + np.exp(-z))


def blrObjFunction(initialWeights, *args):   
    
    train_data, labeli = args

    n_data = train_data.shape[0]
    n_features = train_data.shape[1]
    error = 0.00000
    error_grad = np.zeros((n_features + 1, 1))
    mat = np.zeros((1, n_features + 1))
    mat3 = np.zeros((n_features + 1, 1))
    temp3 = np.zeros((1, n_features + 1))
    
    ##################
    # YOUR CODE HERE #
    ##################
    # Add bias element to input data. A coloumn with with all values = 1 is added at first position.
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    train_data_bias = np.hstack((one_array, train_data))

    # Implement equation 2. this gives scalar value error.
    theta = np.zeros(shape=(n_data,1));
    sum1 = 0;
    temp1 = 0
    temp2 = 0.000000
    count =0

    for i in range(n_data):
        temp3 = train_data_bias[i].reshape((1, 716))
        theta[i] = sigmoid(np.dot(temp3, initialWeights))
        temp1 = labeli[i][0]*np.log(theta[i][0]) + (1-labeli[i][0])*np.log(1-theta[i][0]);
        sum1 = sum1 + temp1

    error = -1*sum1/n_data
    #print "\n sum is " + str(sum1)
    print "error is " + str(error)

    for i in range(0,n_data):
        mat += np.multiply((theta[i]-labeli[i][0]), train_data_bias[i])

    mat3 = np.transpose(mat)
    
    temp2 = 1/train_data_bias.shape[0]    
    error_grad = np.multiply(temp2, mat3)
    error_grad = error_grad.flatten()
    #count = count + 1
    #print "count is  " + str(count)
    
    return error, error_grad

def blrPredict(W, data):
    """
     blrPredict predicts the label of data given the data and parameter W 
     of Logistic Regression
     
     Input:
         W: the matrix of weight of size (D + 1) x 10. Each column is the weight 
         vector of a Logistic Regression classifier.
         X: the data matrix of size N x D
         
     Output: 
         label: vector of size N x 1 representing the predicted label of 
         corresponding feature vector given in data matrix

    """
    label1 = np.zeros((data.shape[0],))
    label = np.zeros((data.shape[0], 1))
    n_data = data.shape[0]
    
    ##################
    # YOUR CODE HERE #
    ##################
    # Add bias element to input data. A coloumn with with all values = 1 is added at first position.
  
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    data_bias = np.hstack((one_array, data))
    print "\n 3 shapes for bias data : "
    print one_array.shape;
    print data.shape;
    print data_bias.shape;
    count2 = 0
    count2 += count2
    print "count2 is  = " + str(count2)

    # predicting labels for data
    theta_all = np.zeros((n_data,10))
    print "theta_all shape"
    print theta_all.shape

    print "\nat start of last for loop"
    for j in range(0, n_data):
        for k in range(10):
            theta_all[j][k] = sigmoid((np.dot(W[:, k],data_bias[j])))
        
        label1 = np.argmax((theta_all), axis=1)
        
    label = label1.reshape((n_data, 1))
    #print " shape of label  "
    #print label.shape
    
    return label


def mlrObjFunction(params, *args):
    """
    mlrObjFunction computes multi-class Logistic Regression error function and
    its gradient.

    Input:
        initialWeights: the weight vector of size (D + 1) x 1
        train_data: the data matrix of size N x D
        Y is an N * K matrix (obtained using 1-of-K encoding) of target variables with elements Ynk

    Output:
        error: the scalar value of error function of multi-class logistic regression
        error_grad: the vector of size (D+1) x 10 representing the gradient of
                    error function
    """
    n_data = train_data.shape[0]
    n_feature = train_data.shape[1]
    error = 0
    error_grad = np.zeros((n_feature + 1, n_class))

    ##################
    # YOUR CODE HERE #
    ##################
    # HINT: Do not forget to add the bias term to your input data

    return error, error_grad


def mlrPredict(W, data):
    """
     mlrObjFunction predicts the label of data given the data and parameter W
     of Logistic Regression

     Input:
         W: the matrix of weight of size (D + 1) x 10. Each column is the weight
         vector of a Logistic Regression classifier.
         X: the data matrix of size N x D

     Output:
         label: vector of size N x 1 representing the predicted label of
         corresponding feature vector given in data matrix

    """
    label = np.zeros((data.shape[0], 1))

    ##################
    # YOUR CODE HERE #
    ##################
    # HINT: Do not forget to add the bias term to your input data

    return label


"""
Script for Logistic Regression
"""
train_data, train_label, validation_data, validation_label, test_data, test_label = preprocess()

# number of classes
n_class = 10

# number of training samples
n_train = train_data.shape[0]

# number of features
n_feature = train_data.shape[1]

Y = np.zeros((n_train, n_class))
for i in range(n_class):
    Y[:, i] = (train_label == i).astype(int).ravel()

# Logistic Regression with Gradient Descent
W = np.zeros((n_feature + 1, n_class))
initialWeights = np.zeros((n_feature + 1, 1))
opts = {'maxiter': 100}
for i in range(n_class):
    labeli = Y[:, i].reshape(n_train, 1)
    args = (train_data, labeli)
    nn_params = minimize(blrObjFunction, initialWeights, jac=True, args=args, method='CG', options=opts)
    W[:, i] = nn_params.x.reshape((n_feature + 1,))

# Find the accuracy on Training Dataset
predicted_label = blrPredict(W, train_data)
print('\n Training set Accuracy:' + str(100 * np.mean((predicted_label == train_label).astype(float))) + '%')

# Find the accuracy on Validation Dataset
predicted_label = blrPredict(W, validation_data)
print('\n Validation set Accuracy:' + str(100 * np.mean((predicted_label == validation_label).astype(float))) + '%')

# Find the accuracy on Testing Dataset
predicted_label = blrPredict(W, test_data)
print('\n Testing set Accuracy:' + str(100 * np.mean((predicted_label == test_label).astype(float))) + '%')

"""
Script for Support Vector Machine
"""

print('\n\n--------------SVM-------------------\n\n')
##################
# YOUR CODE HERE #
##################
X_svm = train_data
y_svm = train_label.ravel()

print('Part 2 SVM.')
print('Linear Kernel.')
clf = SVC(kernel = 'linear')
clf.fit(X_svm, y_svm)
print("Training data accuracy ")
print(clf.score(train_data, train_label))
print("Validation data accuracy ")
print(clf.score(validation_data, validation_label))
print("Testing data accuracy ")
print(clf.score(test_data, test_label))

print('Radial Basis Function, Gamma 1.')
clf = SVC(kernel = 'rbf', gamma = 1)
print('Test.')
clf.fit(X_svm, y_svm)
print("Training data accuracy ")
print(clf.score(train_data, train_label))
print("Validation data accuracy ")
print(clf.score(validation_data, validation_label))
print("Testing data accuracy ")
print(clf.score(test_data, test_label))

print('Radial Basis Function, Gamma auto')
clf = SVC(kernel = 'rbf')
clf.fit(X_svm, y_svm)
print("Training data accuracy ")
print(clf.score(train_data, train_label))
print("Validation data accuracy ")
print(clf.score(validation_data, validation_label))
print("Testing data accuracy ")
print(clf.score(test_data, test_label))

print('Radial Basis Function for different C values\n')
list_iter = [1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

for i in range(len(list_iter)):
    print("For C = " + str(list_iter[i]))
    clf = SVC(C = list_iter[i], kernel = 'rbf')
    clf.fit(X_svm, y_svm)
    print("Training data accuracy ")
    print(clf.score(train_data, train_label))
    print("Validation data accuracy ")
    print(clf.score(validation_data, validation_label))
    print("Testing data accuracy ")
    print(clf.score(test_data, test_label))
    print('\n')


"""
Script for Extra Credit Part
"""
# FOR EXTRA CREDIT ONLY
W_b = np.zeros((n_feature + 1, n_class))
initialWeights_b = np.zeros((n_feature + 1, n_class))
opts_b = {'maxiter': 100}

args_b = (train_data, Y)
nn_params = minimize(mlrObjFunction, initialWeights_b, jac=True, args=args_b, method='CG', options=opts_b)
W_b = nn_params.x.reshape((n_feature + 1, n_class))

# Find the accuracy on Training Dataset
predicted_label_b = mlrPredict(W_b, train_data)
print('\n Training set Accuracy:' + str(100 * np.mean((predicted_label_b == train_label).astype(float))) + '%')

# Find the accuracy on Validation Dataset
predicted_label_b = mlrPredict(W_b, validation_data)
print('\n Validation set Accuracy:' + str(100 * np.mean((predicted_label_b == validation_label).astype(float))) + '%')

# Find the accuracy on Testing Dataset
predicted_label_b = mlrPredict(W_b, test_data)
print('\n Testing set Accuracy:' + str(100 * np.mean((predicted_label_b == test_label).astype(float))) + '%')

pickle.dump( [W], open( "params.pickle", "wb" ))