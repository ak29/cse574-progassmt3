#Multiclass logistic regreession
from __future__ import division
import numpy as np
from scipy.io import loadmat
from scipy.optimize import minimize


def preprocess():
    
    # loads the MAT object as a Dictionary
    mat = loadmat('../Data/mnist_all.mat')

    n_feature = mat.get("train1").shape[1]
    n_sample = 0
    for i in range(10):
        n_sample = n_sample + mat.get("train" + str(i)).shape[0]
    n_validation = 1000
    n_train = n_sample - 10 * n_validation

    # Construct validation data
    validation_data = np.zeros((10 * n_validation, n_feature))
    for i in range(10):
        validation_data[i * n_validation:(i + 1) * n_validation, :] = mat.get("train" + str(i))[0:n_validation, :]

    # Construct validation label
    validation_label = np.ones((10 * n_validation, 1))
    for i in range(10):
        validation_label[i * n_validation:(i + 1) * n_validation, :] = i * np.ones((n_validation, 1))

    # Construct training data and label
    train_data = np.zeros((n_train, n_feature))
    train_label = np.zeros((n_train, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("train" + str(i)).shape[0]
        train_data[temp:temp + size_i - n_validation, :] = mat.get("train" + str(i))[n_validation:size_i, :]
        train_label[temp:temp + size_i - n_validation, :] = i * np.ones((size_i - n_validation, 1))
        temp = temp + size_i - n_validation

    # Construct test data and label
    n_test = 0
    for i in range(10):
        n_test = n_test + mat.get("test" + str(i)).shape[0]
    test_data = np.zeros((n_test, n_feature))
    test_label = np.zeros((n_test, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("test" + str(i)).shape[0]
        test_data[temp:temp + size_i, :] = mat.get("test" + str(i))
        test_label[temp:temp + size_i, :] = i * np.ones((size_i, 1))
        temp = temp + size_i

    # Delete features which don't provide any useful information for classifiers
    sigma = np.std(train_data, axis=0)
    index = np.array([])
    for i in range(n_feature):
        if (sigma[i] > 0.001):
            index = np.append(index, [i])
    train_data = train_data[:, index.astype(int)]
    validation_data = validation_data[:, index.astype(int)]
    test_data = test_data[:, index.astype(int)]

    # Scale data to 0 and 1
    train_data /= 255.0
    validation_data /= 255.0
    test_data /= 255.0

    return train_data, train_label, validation_data, validation_label, test_data, test_label

def sigmoid(z):
	# please refer to sigma value in eqn 1
    return 1.0 / (1.0 + np.exp(-z))

def mlrObjFunction(params, *args):
    """
    mlrObjFunction computes multi-class Logistic Regression error function and
    its gradient.

    Input:
        initialWeights_b: the weight vector of size (D + 1) x 10
        train_data: the data matrix of size N x D
        Y is an N * K matrix (obtained using 1-of-K encoding) of target variables with elements Ynk

    Output:
        error: the scalar value of error function of multi-class logistic regression
        error_grad: the vector of size (D+1) x 10 representing the gradient of
                    error function
    """
    n_data = train_data.shape[0]
    print "n_data of train in obj fn = " + str(n_data)
    n_feature = train_data.shape[1]
    error = 0    
    error_grad = np.zeros((n_feature + 1, 10))

    # number of classes = number of digits to be classified
    n_class = 10
    # number of training samples N
    n_train = train_data.shape[0]

	#number of testing samples
    n_test = test_data.shape[0]

	# number of validation samples
    n_validation = validation_data.shape[0]

	# number of features D+1
    n_features = train_data.shape[1]
    
	# output label matrix

    ##################
    # YOUR CODE HERE #
    ##################
    # Add bias element to input data. A coloumn with with all values = 1 is added at first position.
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    train_data_bias = np.hstack((one_array, train_data))

    # 1-of-k matrix of labels
    Y = np.zeros((n_train, 10))
    for i in range(10):
    	Y[:, i] = (train_label == i).astype(int).ravel()
    for j in range(n_data):
        for k in range(10):
            print Y[j][k]
#imple eqn 5
    theta_mat =  np.zeros(shape=(n_data,10));    
    theta = np.zeros(shape=(n_data,1));
    theta_norm = np.zeros(shape=(n_data,1));
    temp4 = np.zeros((n_features + 1, 1))
    temp3 = np.zeros((1, n_features + 1))

    class_mat0 = np.zeros((1, n_features + 1))
    var0 = np.zeros((n_features + 1, 1))
    class_mat1 = np.zeros((1, n_features + 1))
    var1 = np.zeros((n_features + 1, 1))
    class_mat2 = np.zeros((1, n_features + 1))
    var2 = np.zeros((n_features + 1, 1))
    class_mat3 = np.zeros((1, n_features + 1))
    var3 = np.zeros((n_features + 1, 1))
    class_mat4 = np.zeros((1, n_features + 1))
    var4 = np.zeros((n_features + 1, 1))
    class_mat5 = np.zeros((1, n_features + 1))
    var5 = np.zeros((n_features + 1, 1))
    class_mat6 = np.zeros((1, n_features + 1))
    var6 = np.zeros((n_features + 1, 1))
    class_mat7 = np.zeros((1, n_features + 1))
    var7 = np.zeros((n_features + 1, 1))
    class_mat8 = np.zeros((1, n_features + 1))
    var8 = np.zeros((n_features + 1, 1))
    class_mat9 = np.zeros((1, n_features + 1))
    var9 = np.zeros((n_features + 1, 1))
    
    for j in range(10):
        for i in range(716):
            temp4[j] = initialWeights_b[i][j]
    print "shape of temp4 is  "
    print temp4.shape

    # calculation of normalized theta
    for j in range(n_data):
    	for k in range(10):
            temp3 = train_data_bias[j].reshape((1, 716))
            theta_mat[j][k] = np.exp(np.dot(temp3, temp4))
            print theta_mat[j][k]
            #theta[i] = sigmoid(np.dot(temp3, initialWeights))

    for j in range(n_data):
        for k in range(0,10):
            theta_norm[j] = theta_norm[j] + theta_mat[j][k]

    for j in range(n_data):
    	for k in range(0,10):
    		theta_mat[j][k] = theta_mat[j][k]/theta_norm[j]

    #Impl eqn 7
    for j in range(n_data):
    	for k in range(0,10):
    		error = error + (Y[j][k] * (np.log((theta_mat[j][k]))))
    error = (-1 * error) / n_data
    print error

    #Impl eqn 8

    for j in range(n_data):
    #for k in range(10):        
        class_mat0 = class_mat0 + ((theta_mat[j][0]-Y[j][0]) * train_data_bias[j])
        var0 = np.transpose(class_mat0)

        class_mat1 = class_mat1 + ((theta_mat[j][1]-Y[j][1]) * train_data_bias[j])
        var1 = np.transpose(class_mat1)
        class_mat2 = class_mat2 + ((theta_mat[j][2]-Y[j][2]) * train_data_bias[j])
        var2 = np.transpose(class_mat2)

        class_mat3 = class_mat3 + ((theta_mat[j][3]-Y[j][3]) * train_data_bias[j])
        var3 = np.transpose(class_mat3)
        class_mat4 = class_mat4 + ((theta_mat[j][4]-Y[j][4]) * train_data_bias[j])
        var4 = np.transpose(class_mat4)

        class_mat5 = class_mat5 + ((theta_mat[j][5]-Y[j][5]) * train_data_bias[j])
        var5 = np.transpose(class_mat5)
        class_mat6 = class_mat6 + ((theta_mat[j][6]-Y[j][6]) * train_data_bias[j])
        var6 = np.transpose(class_mat6)

        class_mat7 = class_mat7 + ((theta_mat[j][7]-Y[j][7]) * train_data_bias[j])
        var7 = np.transpose(class_mat7)
        class_mat8 = class_mat8 + ((theta_mat[j][8]-Y[j][8]) * train_data_bias[j])
        var8 = np.transpose(class_mat8)

        class_mat9 = class_mat9 + ((theta_mat[j][9]-Y[j][9]) * train_data_bias[j])
        var9 = np.transpose(class_mat9)

    error_grad = np.concatenate((var0,var1,var2,var3,var4,var5,var6,var7,var8,var9), axis=1)

    print "\n shapes for error_grad  : "
    print error_grad.shape;

    print "some elements of error_grad "
    for j in range(10,12):
        for k in range(1,5):
            print(error_grad[j][k])
    error_grad = error_grad.flatten()

    return error,error_grad
    

def mlrPredict(W, data):
    """
     mlrObjFunction predicts the label of data given the data and parameter W
     of Logistic Regression

     Input:
         W_b: the matrix of weight of size (D + 1) x 10. Each column is the weight
         vector of a Logistic Regression classifier.
         X: the data matrix of size N x D

     Output:
         label: vector of size N x 1 representing the predicted label of
         corresponding feature vector given in data matrix

    """
    label = np.zeros((data.shape[0], 1))
    n_data = data.shape[0]
    print " \n shape of label bfr processing   "
    print label.shape

    ##################
    # YOUR CODE HERE #
    ##################
    # HINT: Do not forget to add the bias term to your input data
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    data_bias = np.hstack((one_array, data))
    print "\n 3 shapes for bias data : "
    print one_array.shape;
    print data.shape;
    print data_bias.shape;
    count2 = 0
    count2 += count2
    print "count2 is  = " + str(count2)

    # predicting labels for data
    theta_all = np.zeros((n_data,10))
    print "theta_all shape"
    print theta_all.shape

    print "\nat start of last for loop"
    for j in range(0, n_data):
        for k in range(10):
            theta_all[j][k] = sigmoid((np.dot(W_b[:, k],data_bias[j])))
        
    label = np.argmax((theta_all), axis=1)

    print " shape of label  "
    print label.shape

    return label


"""
Script for Extra Credit Part
"""
# FOR EXTRA CREDIT ONLY
#get input data from preprocess function
train_data, train_label, validation_data, validation_label, test_data, test_label = preprocess()

# number of classes = number of digits to be classified
n_class = 10
print str(n_class)
# number of training samples N
n_train = train_data.shape[0]

# number of testing samples 
n_test = test_data.shape[0]

# number of validation samples 
n_validation = validation_data.shape[0]

# number of features D+1
n_feature = train_data.shape[1]
print "  n_feature =  " + str(n_feature)

Y = np.zeros((n_train, 10))
for i in range(10):
    Y[:, i] = (train_label == i).astype(int).ravel()

W_b = np.zeros((n_feature + 1, 10))
initialWeights_b = np.zeros((n_feature + 1, 10))
opts_b = {'maxiter': 100}

args_b = (train_data, Y)
nn_params = minimize(mlrObjFunction, initialWeights_b, jac=True, args=args_b, method='CG', options=opts_b)
W_b = nn_params.x.reshape((n_feature + 1, 10))

# Find the accuracy on Training Dataset
predicted_label_b = mlrPredict(W_b, train_data)
print('\n Training set Accuracy:' + str(100 * np.mean((predicted_label_b == train_label).astype(float))) + '%')

# Find the accuracy on Validation Dataset
predicted_label_b = mlrPredict(W_b, validation_data)
print('\n Validation set Accuracy:' + str(100 * np.mean((predicted_label_b == validation_label).astype(float))) + '%')

# Find the accuracy on Testing Dataset
predicted_label_b = mlrPredict(W_b, test_data)
print('\n Testing set Accuracy:' + str(100 * np.mean((predicted_label_b == test_label).astype(float))) + '%')
