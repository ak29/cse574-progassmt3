#test_predict final draft

from __future__ import division
import numpy as np
from scipy.io import loadmat
from scipy.optimize import minimize
import pickle
def preprocess():

    # loads the MAT object as a Dictionary
    mat = loadmat('../Data/mnist_all.mat')

    n_feature = mat.get("train1").shape[1]
    n_sample = 0
    for i in range(10):
        n_sample = n_sample + mat.get("train" + str(i)).shape[0]
    n_validation = 1000
    n_train = n_sample - 10 * n_validation

    # Construct validation data
    validation_data = np.zeros((10 * n_validation, n_feature))
    for i in range(10):
        validation_data[i * n_validation:(i + 1) * n_validation, :] = mat.get("train" + str(i))[0:n_validation, :]

    # Construct validation label
    validation_label = np.ones((10 * n_validation, 1))
    for i in range(10):
        validation_label[i * n_validation:(i + 1) * n_validation, :] = i * np.ones((n_validation, 1))

    # Construct training data and label
    train_data = np.zeros((n_train, n_feature))
    train_label = np.zeros((n_train, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("train" + str(i)).shape[0]
        train_data[temp:temp + size_i - n_validation, :] = mat.get("train" + str(i))[n_validation:size_i, :]
        train_label[temp:temp + size_i - n_validation, :] = i * np.ones((size_i - n_validation, 1))
        temp = temp + size_i - n_validation

    # Construct test data and label
    n_test = 0
    for i in range(10):
        n_test = n_test + mat.get("test" + str(i)).shape[0]
    test_data = np.zeros((n_test, n_feature))
    test_label = np.zeros((n_test, 1))
    temp = 0
    for i in range(10):
        size_i = mat.get("test" + str(i)).shape[0]
        test_data[temp:temp + size_i, :] = mat.get("test" + str(i))
        test_label[temp:temp + size_i, :] = i * np.ones((size_i, 1))
        temp = temp + size_i

    # Delete features which don't provide any useful information for classifiers
    sigma = np.std(train_data, axis=0)
    index = np.array([])
    for i in range(n_feature):
        if (sigma[i] > 0.001):
            index = np.append(index, [i])
    train_data = train_data[:, index.astype(int)]
    validation_data = validation_data[:, index.astype(int)]
    test_data = test_data[:, index.astype(int)]

    # Scale data to 0 and 1
    train_data /= 255.0
    validation_data /= 255.0
    test_data /= 255.0

    return train_data, train_label, validation_data, validation_label, test_data, test_label

def sigmoid(z):
    # please refer to sigma value in eqn 1
    return 1.0 / (1.0 + np.exp(-z))

def blrObjFunction(initialWeights, *args):   
    
    train_data, labeli = args

    n_data = train_data.shape[0]
    n_features = train_data.shape[1]
    error = 0.00000
    error_grad = np.zeros((n_features + 1, 1))
    mat = np.zeros((1, n_features + 1))
    mat3 = np.zeros((n_features + 1, 1))
    temp3 = np.zeros((1, n_features + 1))
    
    ##################
    # YOUR CODE HERE #
    ##################
    # Add bias element to input data. A coloumn with with all values = 1 is added at first position.
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    train_data_bias = np.hstack((one_array, train_data))

    # Implement equation 2. this gives scalar value error.
    theta = np.zeros(shape=(n_data,1));
    sum1 = 0;
    temp1 = 0
    temp2 = 0.000000
    count =0

    for i in range(n_data):
        temp3 = train_data_bias[i].reshape((1, 716))
        theta[i] = sigmoid(np.dot(temp3, initialWeights))
        temp1 = labeli[i][0]*np.log(theta[i][0]) + (1-labeli[i][0])*np.log(1-theta[i][0]);
        sum1 = sum1 + temp1

    error = -1*sum1/n_data
    #print "\n sum is " + str(sum1)
    print "error is " + str(error)

    for i in range(0,n_data):
        mat += np.multiply((theta[i]-labeli[i][0]), train_data_bias[i])

    mat3 = np.transpose(mat)
    
    temp2 = 1/train_data_bias.shape[0]    
    error_grad = np.multiply(temp2, mat3)
    error_grad = error_grad.flatten()
    #count = count + 1
    #print "count is  " + str(count)
    
    return error, error_grad

def blrPredict(W, data):
    """
     blrPredict predicts the label of data given the data and parameter W 
     of Logistic Regression
     
     Input:
         W: the matrix of weight of size (D + 1) x 10. Each column is the weight 
         vector of a Logistic Regression classifier.
         X: the data matrix of size N x D
         
     Output: 
         label: vector of size N x 1 representing the predicted label of 
         corresponding feature vector given in data matrix

    """
    label1 = np.zeros((data.shape[0],))
    label = np.zeros((data.shape[0], 1))
    n_data = data.shape[0]
    
    ##################
    # YOUR CODE HERE #
    ##################
    # Add bias element to input data. A coloumn with with all values = 1 is added at first position.
  
    one_array = np.ndarray([n_data,1])
    one_array.fill('1')
    data_bias = np.hstack((one_array, data))
    print "\n 3 shapes for bias data : "
    print one_array.shape;
    print data.shape;
    print data_bias.shape;
    count2 = 0
    count2 += count2
    print "count2 is  = " + str(count2)

    # predicting labels for data
    theta_all = np.zeros((n_data,10))
    print "theta_all shape"
    print theta_all.shape

    print "\nat start of last for loop"
    for j in range(0, n_data):
        for k in range(10):
            theta_all[j][k] = sigmoid((np.dot(W[:, k],data_bias[j])))
        
        label1 = np.argmax((theta_all), axis=1)
        
    label = label1.reshape((n_data, 1))
    #print " shape of label  "
    #print label.shape
    
    return label

#Script for Logistic Regression"""

#get input data from preprocess function
train_data, train_label, validation_data, validation_label, test_data, test_label = preprocess()

# number of classes = number of digits to be classified
n_class = 10

# number of training samples N
n_train = train_data.shape[0]
"""
# number of testing samples 
n_test = test_data.shape[0]

# number of validation samples 
n_validation = validation_data.shape[0]"""

# number of features D+1
n_feature = train_data.shape[1]
#print "  n_feature =  " + str(n_feature)
count1 =0

# create label matrix.it contains labels in 1-of-k format
Y = np.zeros((n_train, n_class))
for i in range(n_class):
    Y[:, i] = (train_label == i).astype(int).ravel()

# Logistic Regression with Gradient Descent
W = np.zeros((n_feature + 1, n_class))

initialWeights = np.zeros((n_feature + 1, 1))
opts = {'maxiter': 100}

for i in range(n_class):
    count1 += count1
    print "\n\n                                         In for loop should be < 10 : count1 is   "
    labeli = Y[:, i].reshape(n_train, 1)
    args = (train_data, labeli)
    nn_params = minimize(blrObjFunction, initialWeights, jac=True, args=args, method='CG', options=opts)
    W[:, i] = nn_params.x.reshape((n_feature + 1,))

# Find the accuracy on Training Dataset
predicted_label = blrPredict(W, train_data)
print('\n training set Accuracy:' + str(100 * np.mean((predicted_label == train_label).astype(float))) + '%')

# Find the accuracy on Validation Dataset
predicted_label = blrPredict(W, validation_data)
print('\n Validation set Accuracy:' + str(100 * np.mean((predicted_label == validation_label).astype(float))) + '%')

# Find the accuracy on Testing Dataset
predicted_label = blrPredict(W, test_data)
print('\n Testing set Accuracy:' + str(100 * np.mean((predicted_label == test_label).astype(float))) + '%')


pickle.dump( [W], open( "params.pickle", "wb" ))